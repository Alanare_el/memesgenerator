package ru.memesgenerator.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.memesgenerator.models.Meme;
import ru.memesgenerator.repositories.MemeRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemeParser {

    private static final long SECONDS_PER_DAY = 86400;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final MemeRepository memeRepository;
    @Value("${knowyourmeme.pageNumber}")
    private int pageNumber;

    @Autowired
    public MemeParser(MemeRepository memeRepository) {
        this.memeRepository = memeRepository;
    }

    @Scheduled(initialDelay = 1000 * 30, fixedDelay= SECONDS_PER_DAY)
    public void parseKnowYourMemeMemes() {
        for (int i = 1; i < pageNumber; i++) {
            logger.info("Parse Know Your Memes page #{}", i);
            try {
                Document doc = Jsoup.connect("http://knowyourmeme.com/memes/all/page/" + i)
                        .userAgent("Chrome/4.0.249.0 Safari/532.5")
                        .referrer("http://www.google.com")
                        .get();

                Elements links = doc.select(".photo");
                List<String> memesUrls = links.stream()
                        .filter(element -> !element.hasClass("photo left"))
                        .map(element -> element.attr("href"))
                        .collect(Collectors.toList());

                for (String memePageUrl : memesUrls) {
                    Document memeDoc = Jsoup.connect("http://knowyourmeme.com" + memePageUrl)
                            .userAgent("Chrome/4.0.249.0 Safari/532.5")
                            .referrer("http://www.google.com")
                            .get();

                    String memeUrl = memeDoc.select(".social-data").attr("data-media");
                    memeRepository.save(new Meme(memeUrl));
                    logger.info("Save meme Know Your Memes with url #{}", memeUrl);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
