package ru.memesgenerator.services;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.memesgenerator.models.Meme;
import ru.memesgenerator.repositories.MemeRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service
public class MemesService {

    private final MemeRepository memeRepository;
    private final List<Meme> memesList = new ArrayList<>();

    @Autowired
    public MemesService(MemeRepository memeRepository) {
        this.memeRepository = memeRepository;
    }

    @PostConstruct
    private void fillMemesMap() {
        memeRepository.findAll().forEach(memesList::add);
    }

    public byte[] getRandomMeme() {
        double randomIdx = (Math.random() * (-memesList.size())) + memesList.size();

        if (memesList.isEmpty()) return null;

        Meme meme = memesList.get((int) randomIdx);

        try {
            Connection.Response execute = Jsoup.connect(meme.getUrl())
                    .ignoreContentType(true)
                    .userAgent("Chrome/4.0.249.0 Safari/532.5")
                    .referrer("http://www.google.com")
                    .execute();
            return execute.bodyAsBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
