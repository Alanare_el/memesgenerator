package ru.memesgenerator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.memesgenerator.services.MemesService;

@RestController
public class MemesController {

    private final MemesService memesService;

    @Autowired
    public MemesController(MemesService memesService) {
        this.memesService = memesService;
    }

    @GetMapping
    public ResponseEntity<byte[]> getRandomMeme() {
        byte[] image = memesService.getRandomMeme();

        if (image == null) return ResponseEntity.noContent().build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);
        return new ResponseEntity<>(image, headers, HttpStatus.OK);
    }

}
