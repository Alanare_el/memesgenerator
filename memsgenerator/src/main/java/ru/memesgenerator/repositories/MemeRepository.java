package ru.memesgenerator.repositories;


import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;
import ru.memesgenerator.models.Meme;

@Repository
public interface MemeRepository extends KeyValueRepository<Meme, String> { }
