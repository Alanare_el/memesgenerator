package ru.memesgenerator.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

@RedisHash("Meme")
public class Meme implements Serializable {

    @Id
    @Indexed
    private final String url;
    private long viewsCount;

    public Meme(String url) {
        this.url = url;
    }

    /**
     * Method for getting meme unique identifier, also url.
     * @return meme unique identifier.
     */
    public String getUrl() {
        return url;
    }

    public long incrementViewsCount() {
        return ++this.viewsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meme)) return false;

        Meme meme = (Meme) o;

        if (viewsCount != meme.viewsCount) return false;
        return getUrl() != null ? getUrl().equals(meme.getUrl()) : meme.getUrl() == null;
    }

    @Override
    public int hashCode() {
        int result = getUrl() != null ? getUrl().hashCode() : 0;
        result = 31 * result + (int) (viewsCount ^ (viewsCount >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Meme{" +
                "url='" + url + '\'' +
                ", viewsCount=" + viewsCount +
                '}';
    }
}
