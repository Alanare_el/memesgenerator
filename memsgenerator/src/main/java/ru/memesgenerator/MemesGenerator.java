package ru.memesgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemesGenerator {

    public static void main(String[] args) {
        SpringApplication.run(MemesGenerator.class, args);
    }

}
